# Asset Database

The asset database is an in-memory index that SuperBLT keeps about all the game's assets. This allows
it to quickly read game assets, regardless of whether or not they've been loaded by a package.

This is mostly used by the Wren asset loading system, but is also accessible from Lua.

Note: the name/type/language for these functions are strings, not `Idstring` objects. If you have the
hash but don't know the original name, a 16-byte hex-encoded hash proceeded by a hash character is
supported, same as the Wren asset functions.
Hashes obtained in-game from `Idstring` objects need to be followed by an exclamation mark (`!`)
for use with the asset database since they need to be converted to big endian.

## Read a file

```lua
function string blt.asset_db.read_file(string name, string type, table or nil options {
  optional boolean optional -- If true, this function will return nil rather than raising an error if the file does not exist
  optional string language -- If set, the asset must have this language tag. Otherwise the asset must not have a language tag.
})
```

Reads all the bytes in an asset and returns them as a string.

This asset does not have to be loaded by the game, so you can use it to access any file.

## Check if a file exists

```lua
function boolean blt.asset_db.has_file(string name, string type, table or nil options {
  optional string language -- If set, the asset must have this language tag. Otherwise the asset must not have a language tag.
})
```

Checks if `read_file` would succeed for the given parameters. This may seem redundant since `DB:has` exists, but
there are a few differences:

* `DB:has` uses the game's current language while for reading a file you must specify it if the asset is localised
* `read_file` fails for assets loaded with `DB:create_entry`, while `DB:has` returns true for them.
