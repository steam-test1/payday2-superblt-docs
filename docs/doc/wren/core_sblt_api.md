# Core SuperBLT API

This is a little bit of an accident of history, however it contains some very important
basic functions.

# Logger

A rather simple class with only one function - `log` - which prints a single string to
the command line. Note this must be a string.

This is the equivalent of the `log` call in Wren.

```wren
import "base/native" for Logger
Logger.log("Hello, World")
```

Note: the standard Wren `System.print` functions currently don't do anything, though this
is something that will hopefully change in the future.

# IO

This class has the following functions:

## List Directory

```wren
foreign static listDirectory(path, dirs)
```

This function returns an array of strings, one per item in the directory specified by the `path` argument (which is
relative to the game EXE, same as all the Lua IO functions).

If the `dirs` argument is true, the returned entries are all directories; otherwise they're files.

Example:

```wren
IO.listDirectory("mods", true)
// Returns ["base", "logs", "saves"]

IO.listDirectory("mods", false)
// Returns ["developer.txt"]
```

## File Info

```wren
foreign static info(path)
```

This returns the type of a given file - currently either `none`, `file` or `dir`.

Example:

```wren
IO.info("mods")
// Returns "dir"

IO.info("context.xml")
// Returns "file"

IO.info("does-not-exist.txt")
// Returns "none"
```

## Read file contents

```wren
foreign static read(path)
```

Returns the contents of a given file in string form.

WARNING: Only use this on text files! Wren requires that all strings are in UTF-8 format, so performing
any kind of string operations of the results returned by a binary file will crash the game. This is really
playing with fire, so please just don't do it.

## IDString hash

```wren
foreign static idstring_hash(data)
```

Returns (in string form) the lookup8 hash of a given string. This is the same hash system that `Idstring`
uses, and is compatible with it. The output is always 16 hexadecimal characters.

Example:

```wren
IO.idstring_hash("hi")
// Returns "3e03d9a1c5100287"
```

## Load Native Plugin

```wren
foreign static load_plugin(filename)
```

Loads a DLL (Windows) or SO (Linux) file into the game. This should rarely if ever be done by mods - you can
and probably should just specify it in the `supermod.xml` file like so:

```xml
<?xml version="1.0"?>
<mod>
	<native_module platform="mswindows" filename="my-file.dll" />
</mod>
```

## Check for native Wren module

```wren
foreign static has_native_module(name)
```

Checks if the given file is a built-into-DLL wren module. This is useful for making code that doesn't
run on older versions of SuperBLT.

Example:

```wren
IO.has_native_module("base/native/DB_001")
// Returns true

IO.has_native_module("base/native/some_module_that_doesnt_exist")
// Returns false
```

## Dynamic Module Import

```wren
static dynamic_import(path)
```

This loads and runs a given Wren module, as if you'd specified it in an import statement.

The reason for using this is that unlike import statements, you can use variables for it's path, make
it conditional on something (see `has_native_module`) or put it in a fibre which you call `try` on.

Unlike a normal import statement you cannot bind to it's variables, however.

This follows all the standard [import rules](modules_and_importing.md), same as any normal import statement.

Example:

```wren
if (IO.has_native_module("base/native/some_future_module") {
    IO.dynamic_import("./some_nifty_new_feature_module")
}
```

# XML Parsing

TODO write all this - it's quite long, and for now read `base/wren/private/xml_parser.wren` in the base mod for
an example of how to use this.
