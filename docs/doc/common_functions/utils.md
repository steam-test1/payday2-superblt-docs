# Utility Functions

## Values

Functions to check or convert values.

### Logging a table

```lua
Utils.PrintTable(tbl, maxDepth)
```

Prints the contents of a table to the console and log file.  
May cause game slowdown if the table is fairly large, use only for debugging purposes.  
`tbl` The table to print to console.  
`maxDepth` Controls the depth that PrintTable will read to (defaults to `1`).  

```lua
local test_table = {
  1,
  2,
  "345",
  ["hello"] = "test"
}
Utils.PrintTable(test_table)
```

### Saving a table to a file

```lua
Utils.SaveTable(tbl, file)
```

Saves all contents of the specified table to the file specified. The file will be created if it doesn't exist, and any existing contents will be overwritten.  
`tbl` The table to save to a file.  
`file` The path and name of the file to save the table contents to, relative to PAYDAY 2 directory.  

```lua
  SaveTable(MenuManager, "menu_manager.txt")
```

### Improved string conversion

```lua
Utils.ToString(value, maxDepth)
```

Returns a string representation of a value.  
If value is a table, `ToString` returns a string representing the table and its contents.  
`value` Any value.  
`maxDepth` Controls the depth that `ToString` will read to (defaults to `1`).  
`returns` A string representation of the value.  

Unlike `tostring(value)`, this utility function also handles table values and converts them into human-readable representations.

### Checking if a string is empty

```lua
string.is_nil_or_empty(str)
```

Checks if the specified string `str` contains any data.  
`str` The string to check contains any data.  
`returns` True if `str` is nil or empty (_""_), false if it contains any data.  

```lua
local s = "hello world!"
string.is_nil_or_empty(s)
--> false
```

### Rounding to a number of decimal places

```lua
math.round_with_precision(num, precision)
```

Rounds a number `num` to the specified number of decimal places in `precision`.  
`num` The number to round.  
`precision` The number of decimal places to round `num` to.  

```lua
local num = 12.54135483153
math.round_with_precision(num, 3)
--> 12.541
```

### Toggle menu item to boolean

```lua
Utils:ToggleItemToBoolean(item)
```

Gets the string value of a toggle menu item and converts it to a boolean value.  
`item` The toggle menu item to get a boolean value from.  
`returns` True if the toggle item is on, false otherwise.  

### URL sanitizing

```lua
Utils:EscapeURL(input_url)
```

Escapes special characters in a URL to turn it into a usable URL.  
`input_url` The url to escape the characters of.  
`returns` A url string with escaped characters.  

### Timestamp from a date

```lua
Utils:TimestampToEpoch(year, month, day)
```

Converts a date given by year, month and day into a timestamp.  
`year` Year of the date.  
`month` Month of the date.  
`day` Day of the date.  
`returns` The timestamp.  

### Getting the first non-nil value

```lua
Utils:FirstNonNil(...)
```

Returns the first value in the list of arguments that isn't `nil`.  
`...` List of values.  
`returns` First value that isn't `nil`, or `nil` if all of them are.  

Example:

```lua
Utils:FirstNonNil(nil, 10, nil, 1)
--> 10
```

### Handling nested table values

Since Lua does not have null-conditional operators, checking for values nested inside tabled can become quite cumbersome,
especially if you have to check every key before accessing it.
Imagine a table like the following:

```lua
local my_table = {
  a = {
    b = {
      c = "d"
    }
  }
}
```

If you want to safely access `c` you would do something like this:

```lua
if my_table.a and my_table.a.b and my_table.a.b.c then
  log(c)
end
```

Especially with longer key names this can quickly get very messy.
That's where the nested table utility functions come in handy.

#### Retrieving a nested value

```lua
Utils:GetNestedValue(tbl, ...)
```

Retrieves a value from a nested table by a sequence of keys.  
`tbl` The nested table to retrieve the value from.  
`...` A sequence of keys to traverse the nested table.  
`returns` The value found in the nested table, or `nil` if any key in the sequence is not found.  

Example:

```lua
local c = Utils:GetNestedValue(my_table, "a", "b", "c")
```

#### Setting a nested value

```lua
Utils:SetNestedValue(tbl, val, ...)
```

Sets a value in a nested table by a sequence of keys.  
Creates keys if needed but will not override existing keys that don't hold table values.  
`tbl` The nested table to set the value in.  
`val` The value to set in the table.  
`...` A sequence of keys to traverse the nested table.  
`returns` True if the value was set, or false if any key in the sequence doesn't hold a table value.  

Example:

```lua
Utils:SetNestedValue(my_table, "foo", "a", "b", "c")
```

### Checking object class

```lua
Utils:IsInstanceOf(object, c)
```

Checks if the given object is an instance of the specified class or a subclass of it.  
`object` The object to check.  
`c` The class to check against.  
`returns` True if the object is an instance of the specified class or subclass of it, false otherwise.  

Example:

```lua
local player = managers.player:local_player()
Utils:IsInstanceOf(player:character_damage(), PlayerDamage)
--> true
```

## Game State

Functions to check for specific game states.

### Checking if you're ingame

```lua
Utils:IsInGameState()
```

Returns whether you are in GameState (loadout, ingame, end screens like victory and defeat) or not.  
`returns` True if you are in GameState, false otherwise.  

### Checking if you're loading

```lua
Utils:IsInLoadingState()
```

Returns wether you are currently in a loading state or not.  
`returns` True if you are in a loading state, false otherwise.  

### Checking if you're in heist

```lua
Utils:IsInHeist()
```

Returns wether you are currently in game (you're able to use your weapons, spot, call teammates etc) or not.  
Only returns true if currently ingame, does not check for GameState like `Utils:IsInGameState()`.  
`returns` True if you are in game, false otherwise.  

### Checking if you're in custody

```lua
Utils:IsInCustody()
```

Returns whether you are currently in custody or not.  
`returns` True if you are in custody, false otherwise.  

## Player

Player specific utility functions.

### Checking if primary is of a category

```lua
Utils:IsCurrentPrimaryOfCategory(category)
```

Checks current primary weapon's weapon category.  
`category` The weapon category to check for (refer to weapontweakdata.lua).  
`returns` True if the weapon has `category` as category, false otherwise.  

### Checking if secondary is of a category

```lua
Utils:IsCurrentSecondaryOfCategory(category)
```

Checks current secondary weapon's weapon category.  
`category` The weapon category to check for (refer to weapontweakdata.lua).  
`returns` True if the weapon has `category` as category, false otherwise.  

### Check current weapon

```lua
Utils:IsCurrentWeapon(id)
```

Checks if a specific weapon is currently equipped.  
`id` The weapon's name ID (refer to weapontweakdata.lua).  
`returns` True if the currently equipped weapon matches `id`, false if not and `nil` if no weapon is equipped.  

### Check if primary is equipped

```lua
Utils:IsCurrentWeaponPrimary()
```

Checks if the currently equipped weapon is your primary weapon.  
`returns` True if the current weapon is a primary, false if not and `nil` if no weapon is equipped.  

### Check if secondary is equipped

```lua
Utils:IsCurrentWeaponSecondary()
```

Checks if the currently equipped weapon is your secondary weapon.  
`returns` True if the current weapon is a secondary, false if not and `nil` if no weapon is equipped.  

### Getting the player aim position

```lua
Utils:GetPlayerAimPos(player_unit, maximum_range)
```

Gets the point in the world where the player is aiming at as a Vector3.  
`player_unit` The player unit to get the aiming position of (defaults to the local player).  
`maximum_range` The maximum distance to check for a point in cm (defaults to `100000`).  
`returns` A Vector3 containing the location that the player is looking at, or `nil` if the player was not looking at anything or was looking at something past the maximum_range.  

### Raycast between two points

```lua
Utils:GetCrosshairRay(from, to, slot_mask)
```

Gets a ray between two points and checks for a collision with a slot mask along the ray.  
`from` The starting position of the ray (defaults to the current camera position).  
`to` The ending position of the ray (defaults to 200m in the current camera's look direction).  
`slot_mask` The collision group to check against the ray (defaults to all objects the player can shoot).  
`returns` A table containing the ray information or `nil` if no viewport camera exists.  
