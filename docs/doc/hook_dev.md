# Getting started with hook development

Before you can do any work on the hook, you first need to be able to compile your changes and
run them inside PAYDAY 2. This document explains how to do that, on both Windows and Linux.

# Windows Setup

First clone the repo.

Next, Download all the submodules with `git submodule update --init --recursive`. If you have
weird issues after pulling new changes, it may be necessary to run this again.

Next open it in your IDE of choice. In CLion, select `File -> Open` and the folder created
by git clone. For Visual Studio, select `File -> Open -> CMake` and select the top-level
`CMakeLists.txt` file.

Next set your IDE to build a 32-bit binary. In CLion first ensure you have a x86 toolchain
configured, in `File | Settings | Build, Execution, Deployment | Toolchains` - add a new
Visual Studio toolchain and set the architecture to `x86`. Ensure this toolchain is selected
in `File | Settings | Build, Execution, Deployment | CMake`, in your `Debug` configuration and
inside the toolchain box. Once CMake is done, select `WSOCK32` in the target box.

To enable multithreaded builds in CLion (which massively improves build times) you need to
download [JOM](https://wiki.qt.io/Jom). In the toolchain you added earlier, set the `Make`
field to the path of the `jom.exe` file you extracted earlier.

In Visual Studio, select the Configuration box (at the top of the window, which may for
example say `x64-Debug`) and select `Manage Configurations...`. Remove the existing
configuration then add a new one, and select `x86-Debug`. Select `Project->Generate Cache`
and wait for it to run cmake - this may take some time.

At this point you can compile your project. In CLion, Click the little green hammer in the top-right
of CLion (the shortcut varies depending on platform). In Visual Studio, press F7. In either case this
will take some time as it compiles all of SuperBLT's dependencies, and finally SuperBLT itself.

Finally, you need to make PAYDAY use your custom-built version of SBLT. Go to your `PAYDAY 2`
directory and open PowerShell. Run:

```
cmd /c mklink WSOCK32.dll <path to SBLT>\out\build\x86-Debug\WSOCK32.dll
```

for Visual Studio, or:

```
cmd /c mklink WSOCK32.dll <path to SBLT>\cmake-build-debug\WSOCK32.dll
```

for CLion.

At this point you can run the game, and it should use your custom SBLT build. If you want to check
that it worked, go to `platforms/w32/platform.cpp:InitPlatform` and add:

```
MessageBoxW(0, L"Hello", L"Testing, 123", MB_OK);
```

(you'll also have to add `#include <Windows.h>` earlier in the file)

Compile then run the game, and you should see a popup box appear on startup.

## Linux

TODO write this up.
