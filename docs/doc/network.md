# Lua networking

A collection of helper Lua functions to allow mods to send networked data to other players who have the mod installed.
All data is sent as string data between clients, so any non-string data, such as numbers or Vector3's, must be converted into a string before transmission.

## Networking information

### LuaNetworking:IsMultiplayer()

Checks if the game is in a multiplayer state, and has an active multiplayer session.  
`returns` The active multiplayer session, or _nil_.  

```lua
if LuaNetworking:IsMultiplayer() then
  log("Multiplayer!")
else
  log("Single-player!")
end
```

### LuaNetworking:IsHost()

Checks if the local player is the host of the multiplayer game session.  
`returns` True if the local player is the host of the game session, or false. Also returns false if not multiplayer session is running.  

```lua
if LuaNetworking:IsHost() then
  log("I am the host!")
end
```

### LuaNetworking:IsClient()

Checks if the local player is a client of the multiplayer game session.  
`returns` True if the local player is not the host of the game session, or false. Also returns false if not multiplayer session is running.  

```lua
if LuaNetworking:IsClient() then
  log("I am a client!")
end
```

### LuaNetworking:LocalPeerID()

Returns the peer ID of the local player.  
`returns` The peer ID of the local player in the current multiplayer session. Returns _0_ if no session was found.  

```lua
local peer_id = LuaNetworking:LocalPeerID()
```

### LuaNetworking:GetNameFromPeerID(id)

Returns the name of the player associated with the specified peer ID `id`.  
`id` The peer ID to lookup in the session players and get their name.  
`returns` The name of the player with peer ID `id`. Returns _No Name_ if a name could not be found the specified ID.  

```lua
local my_id = LuaNetworking:LocalPeerID()
local my_name = LuaNetworking:GetNameFromPeerID(my_id)
log("My name is: " .. my_name)
```

### LuaNetworking:GetPeers()

An accessor for the session peers table.  
`returns` The table of all connected peers in the current multiplayer session.  

```lua
for id, ply in pairs(LuaNetworking:GetPeers()) do
  log(id .. " = " .. LuaNetworking:GetNameFromPeerID(id))
end
```

### LuaNetworking:GetNumberOfPeers()

Utility function for quickly and easily getting the number of players in the multiplayer session.  
`returns` The number of connected players in the current session.  

```lua
local num = LuaNetworking:GetNumberOfPeers()
log("There are " .. tostring(num) .. " connected players.")
```

## Sending data

### LuaNetworking:SendToPeers(id, data)

Sends networked data `data` with a message id of `id` to all connected players.  
`id` The unique identifier of the data to send to connected players. This value shoud be unique to the data type you are sending, or to the remote function you wish to call on another players game.  
`data` The data to send to all connected players. It should be in string format so that it will be transmitted to other players properly.  

```lua
LuaNetworking:SendToPeers("GoonModCustomLaserColour", "1.0000000,0.0000000,1.0000000")
```

### LuaNetworking:SendToPeer(peer, id, data)

Identical to `LuaNetworking:SendToPeers`, except the first argument is the ID of the peer who should receive the data.  
`peer` The peer ID of the player who should receive the networked data.  
`id` The unique identifier of the data to send to the peer with id `id`.  
`data` The data to send.  

```lua
local message_text = "This is a private message to the player with Peer ID 1."
LuaNetworking:SendToPeer(1, "PrivateMessage", message_text)
```

### LuaNetworking:SendToPeersExcept(peer, id, data)

Identical to `LuaNetworking:SendToPeer`, except the first argument is a peer ID, or table or peer ID's who should be excluded from receiving the networked data.  
`peer` A peer ID, or a table of peer ID's, who should be excluded from receiving this networked data.  
`id` The unique identifier of the data to send to the peer with id `id`.  
`data` The data to send.  

```lua
local message_text = "This message will be send to everybody who does not have a Peer ID of 4."
LuaNetworking:SendToPeersExcept(4, "PrivateMessage", message_text)

local exclude = { 1, 2 }
local message_text = "This message will be send to everybody who's peer ID does not appear in the exlude table"
LuaNetworking:SendToPeersExcept(exclude, "PrivateMessage", message_text)
```

## Receiving data

A specific hook is called when a Lua-network event is received from another player. In order to make use of the data received from other players, your mod should add to the hook in order to process the data received and call the appropriate functions required.

For more information on listening to hooks, see the main [Hooks](common_functions/hooks.md) page.

### NetworkReceivedData(sender, id, data)

The name of the hook that is called everytime any data is received via LuaNetworking from another player.  
`sender` The peer ID of the player who sent the networked data.  
`id` The unique identifier of the data sent.  
`data` The data sent to us, as a string. It may have to be converted to another data type in order for us to make use of it.  

```lua
local private_message_id = "PrivateMessage"
Hooks:Add("NetworkReceivedData", "NetworkReceivedData_PMs", function(sender, id, data)

  if id == private_message_id then

    local name = Net:GetNameFromPeerID(sender)
    log("Received Private Message from: " .. name)
    log("Message: " .. data)

  end

end)
```

As we can see in our example above, once subscribed to our hook we need to check the ID of the data we receive to make sure that the data is relevant to our mod.
If the data we receive is not relevant, then we should not process or call anything, as another mod is likely handling it instead.

## Extensions

Various extension and helper functions for converting data types that may not normally be convertible into a string, into a string and back into their original type.

### Converting tables

#### Table to string

```lua
LuaNetworking:TableToString(tbl)
```

Converts a table into a string which is able to sent through the Networking functions. It will only perform a shallow conversion, meaning it should not be used with nested tables.  
`tbl` The table to be converted into a string.  
`returns` A string representation of the table passed into the function.  

```lua
local tbl = { 1, 2, 3, 4, "hello world!" }
local str = Net:TableToString(tbl)
LuaNetworking:SendToPeers("NetworkTableStringTest", str)
```

#### String to table

```lua
LuaNetworking:StringToTable(str)
```

Converts a formatted string into a table. Should be used for turning a networked table sent as a string back into its original table.  
`str` The formatted string to attempt to convert back into a table.  
`returns` A table containing the formatted data from `str`.  

```lua
local str = "key|value,key2|another value,another key|yet another value"
local tbl = LuaNetworking:StringToTable(str)
```

### Converting colors

#### Color to string

```lua
LuaNetworking:ColourToString(col)
```

Converts a colour into a formatted string, which is able to be sent through the Networking functions.  
`col` The colour to be converted into a formatted string.  
`returns` A formatted string containing the colour data from `col`.  

```lua
local color = Color.red
local col_str = LuaNetworking:ColourToString(color)
LuaNetworking:SendToPeers("NetworkColourStringTest", col_str)
```

#### String to color

```lua
LuaNetworking:StringToColour(str)
```

Converts a formatted string into a colour. Should be used for turning a networking colour sent as a string back to its original object.  
`str` The formatted string to attempt to convert back into a Color object.  
`returns` A Color object containing the original colour.  

```lua
  local col_str = "r:1.0000|g:0.2125|b:0.4568|a:1.0000"
  local color = LuaNetworking:StringToColor(col_str)
```

### Converting vectors

#### Vector to string

```lua
LuaNetworking:Vector3ToString(v)
```

Converts a Vector3 to a formatted string.  
`vec` The Vector3 to convert to a string.  
`returns` A formatted string with the information of the input `vec`.  

```lua
local vec = Vector3(50, 0, 10)
LuaNetworking:Vector3ToString(vec)
```

#### String to vector

```lua
LuaNetworking:StringToVector3(string)
```

Converts the formatted string `str` into a usable Vector3.  
`str` The formatted string to convert into a Vector3.  
`returns` A Vector3.  

```lua
local vec_str = "50.000000,25.456841,00.000000"
LuaNetworking:StringToVector3(vec_str)
```
