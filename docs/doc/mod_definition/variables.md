# Mod related variables

These are global variables that may change depending on the context they are checked in.

## RequiredScript

A variable containing the name and path of the Payday lua file that will be run, or was previously run.
This is a path as specified in Hooks or Pre-Hooks in the [mod definition file](basics.md).

When hooking multiple game files to the same mod file, this can be used to check which file is currently being executed.

```lua
if RequiredScript == "lib/managers/menumanager" then
  -- do MenuManager stuff
elseif RequiredScript == "lib/managers/playermanager" then
  -- do PlayerManager stuff
end
```

## LogsPath

A variable containing the path to the mods logs folder.  

```lua
log(LogsPath)
--> "mods/logs/"
```

## SavePath

A variable containing the path to the save files folder.  

```lua
log(SavePath)
--> "mods/saves/"
```

## ModPath

A variable containing the path of the mod that is currently being loaded. This value should be cached if it is
intended to be used at some point in the future, such as in a Hook.  

```lua
local my_mod_path = ModPath
log(my_mod_path)
--> "mods/my_example_mod/"
```

## ModInstance

A variable containing the currently executed `BLTMod` instance. Just like `ModPath`, this value should be cached if it is
intended to be used at some point in the future.
