# Automatic updates

Automatic updates provides people that are using your mod with a convenient way to update your mod
without having to manually go to a website to download the latest version.
Automatic updates consist of two parts, an update definition in your [mod definition file](basics.md)
and a meta file that can be accessed remotely to allow SuperBLT to check for updates.

## Update definition

In your mod.txt file, simply add an update definition like this:

```json
"updates" : [
  {
    "identifier" : "anythingyouwant",
    "host": {
      "meta": "https://example.com/my_meta.json",
      "download": "https://example.com/download.zip",
      "patchnotes": "https://example.com/patchnotes.html"
    }
  }
]
```

The identifier can be anything you like - issues will arise if multiple mods have the same
identifier, so set it to something that you can reasonably assume no other mod will use.
Providing the `download` and `patchnotes` link is optional, if they are not present
SuperBLT will simply try and get them from the meta file.
If you don't provide them in your mod.txt you must provide them in the meta file.

## Meta file

The meta file is a JSON formatted file that contains one or more update definitions.
This file must be available online for SuperBLT to check the current mod version against
the latest one provided in the meta file.

One simple way to provide access to your meta file without having to self-host it somwhere is putting your mod
on [GitHub](https://github.com/), including the meta file with it and simply putting the raw GitHub link
of the meta file into the `host` section of your mod.txt.

The meta file should be in the following format:

```json
[
  {
    "ident": "anythingyouwant",
    "version": "1.0",
    "patchnotes_url": "https://example.com/other-patchnotes.html",
    "download_url": "https://example.com/other-download.zip"
  }
]
```

The identifier in the meta file must be the same as that in the update tag in your mod.txt.
The version tells SuperBLT what the newest version of your mod and will be compared to the
version in the mod.txt to determine if there is an update.

The `patchnotes_url` and `download_url` values override the `patchnotes` and `download` values in the mod.txt if supplied.

### Hash-based updating

Instead of providing a version it is also possible to provide a hash value of the mod.
It is generally recommended to use version based updating as that is much simpler to set up
and doesn't require any other external tools.
Should you still wish to do hash-based update checks, you simply replace the version entry in the meta file with the current hash:

```json
[
  {
    "ident": "anythingyouwant",
    "hash": "43e7cd36567c755be88e60bde45ba418527c692af982d45fbedb8b5a8c792772"
  }
]
```

The hash is a SHA-256 hash that is built by first creating the hashes of all individual files
and then concatenating all of these file hashes in alphabetical (based on their original names) order
and creating the final hash from that.

While checking for updates, SuperBLT will then calculate the hash of the current mod and compare it with the hash in the meta file.
