# Dependencies

Sometimes a mod may require another mod to function and while you can tell your
users to install all required mods themselves, setting up your mod dependencies
and letting SuperBLT handle the notifications and downloads for you can be the more
comfortable alternative.

In your mod.txt file, adding dependencies looks like this:

```json
"dependencies" : {
  "Mod Name" : { 
    "download_url" : "https://example.com/mod.zip"
  },
  "mod-identifier" : {
    "meta" : "https://example.com/meta.json"
  }
}
```

As you can see, there are two ways of defining dependencies - by using the mod name
as defined in the dependency's mod.txt or the mod identifier used in the updates section of the `mod.txt`.
Furthermore you can either link to a direct download link by specifying a `download_url`
or link to the `meta` file that is also used for automatically updating that mod.
When using the `meta` file, make sure that your dependency name is the mod identifier that is used in
the `meta` file of said dependency or it will fail to match the mod with the received `meta` file.

The dependency data can contain additional optional entries like `name` to specify
the display name in the download manager and the mod folder name after download or `install_dir`
to specify which folder the mod goes to after downlaod (defaults to mods).
These values will also be taken from a `meta` file if they are provided there.

Finally, if your mod dependency only consists of a download link and you don't need any additional
data, you can take the following shortcut for defining the dependency:

```json
"dependencies" : {
  "Mod Name" : "https://example.com/mod.zip",
}
```

This will be treated the same way as the `"Mod Name"` dependency in the first example.
